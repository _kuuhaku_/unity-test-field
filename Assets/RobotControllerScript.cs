﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RobotControllerScript : MonoBehaviour {

    public GameObject Robot;
    float direction = 1;
	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update ()
    {
        if (Robot.GetComponent<Rigidbody2D>().velocity.x < 3 && Robot.GetComponent<Rigidbody2D>().velocity.x > -3)
        {
            Robot.GetComponent<Rigidbody2D>().AddForce(new Vector2(direction * 0.5f, 0),ForceMode2D.Impulse);
        }
    }

    public void Collide()
    {
        direction = -1 * direction;
    }
}
