﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RobotColliderScript : MonoBehaviour {

    public GameObject Robot;
    
    void OnTriggerEnter2D(Collider2D other)
    {
        Robot.GetComponent<RobotControllerScript>().Collide();
    }
}
